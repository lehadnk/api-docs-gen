<?php
/**
 * Created by PhpStorm.
 * User: lehadnk
 * Date: 2019-02-02
 * Time: 20:56
 */

namespace UnitTests\Transformers;

use ApiDocsGenerator\DataCollectors\FileDataCollector;
use ApiDocsGenerator\FileLister;
use UnitTests\AbstractTestProjectCase;

class FileDataCollectorTest extends AbstractTestProjectCase
{
    public function testFileDataCollector()
    {
        $collector = new FileDataCollector($this->getTestProjectDir().'/response-data.json');
        $data = $collector->getRequestData();
        $this->assertEquals('123', $data->headers['X-Header']);
        $this->assertEquals('test', $data->content);
        $this->assertEquals(200, $data->status);
    }
}