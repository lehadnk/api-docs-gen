<?php
/**
 * Created by PhpStorm.
 * User: lehadnk
 * Date: 2019-02-02
 * Time: 09:38
 */

use ApiDocsGenerator\Config\ArrayConfigReader;
use ApiDocsGenerator\Transformers\SectionTransformer;
use UnitTests\AbstractTestProjectCase;

class ArrayConfigReaderTest extends AbstractTestProjectCase
{

    public function testRead()
    {
        $reader = new ArrayConfigReader();
        $config = $reader->read($this->getTestProjectDir().'/array-config.php');

        $this->assertContains('ru', $config->languages);
        $this->assertContains('en', $config->languages);
        $this->assertContains('cn', $config->languages);

        $this->assertArrayHasKey('user-api', $config->sections);
        $this->assertArrayHasKey('merchant-api', $config->sections);

        $this->assertArrayHasKey('api_token', $config->sections['user-api']->securityDefinitions);

        $this->assertEquals('User API', $config->sections['user-api']->title);
    }
}
