<?php
/**
 * Created by PhpStorm.
 * User: lehadnk
 * Date: 2018-11-28
 * Time: 17:35
 */

namespace UnitTests;

use ApiDocsGenerator\Config\ArrayConfigReader;
use ApiDocsGenerator\Config\GeneratorConfig;
use PHPUnit\Framework\TestCase;

abstract class AbstractTestProjectCase extends TestCase
{
    protected function getTestProjectDir()
    {
        return __DIR__.'/../data';
    }

    protected function getTestConfig(): GeneratorConfig
    {
        $configReader = new ArrayConfigReader();
        return $configReader->read($this->getTestProjectDir().'/array-config.php');
    }

    protected function removeFile(string $fileName)
    {
        $fileName = $this->getTestProjectDir().'/'.$fileName;
        if (file_exists($fileName)) {
            unlink($fileName);
        }
    }
}
