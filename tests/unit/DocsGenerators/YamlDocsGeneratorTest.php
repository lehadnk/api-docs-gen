<?php
/**
 * Created by PhpStorm.
 * User: lehadnk
 * Date: 2019-02-02
 * Time: 20:56
 */

namespace UnitTests\Transformers;

use ApiDocsGenerator\DocsGenerators\YmlDocsGenerator;
use ApiDocsGenerator\FileLister;
use UnitTests\AbstractTestProjectCase;

class YamlDocsGeneratorTest extends AbstractTestProjectCase
{
    public function tearDown()
    {
        $this->removeFile('generated.php');
    }

    public function testRun()
    {
        $generator = new YmlDocsGenerator();
        $generator->generate($this->getTestProjectDir().'/docs.yml', $this->getTestProjectDir().'/generated.php');

        $this->assertTrue(file_exists($this->getTestProjectDir().'/generated.php'));
    }
}