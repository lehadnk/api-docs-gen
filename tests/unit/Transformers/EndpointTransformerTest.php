<?php
/**
 * Created by PhpStorm.
 * User: lehadnk
 * Date: 2019-02-02
 * Time: 20:56
 */

namespace UnitTests\Transformers;

use ApiDocsGenerator\DataCollectors\PlainTextDataCollector;
use ApiDocsGenerator\Endpoints\ApiEndpoint;
use ApiDocsGenerator\RequestParameters\StringRequestParameter;
use ApiDocsGenerator\Transformers\SectionTransformer;
use UnitTests\AbstractTestProjectCase;

class EndpointTransformerTest extends AbstractTestProjectCase
{
    public function testRun()
    {
        $config = $this->getTestConfig();
        $sectionConfig = $config->sections['user-api'];

        $apiEndpoint = new ApiEndpoint([
            'method' => 'GET',
            'uri' => '/events/1',
            'description' => 'sadsa',
            'summary' => 'test',
            'inputParams' => [
                new StringRequestParameter('name', 'Test descr'),
            ],
            'responseExamples' => [
                new PlainTextDataCollector(200, 'test response'),
            ]
        ]);

        $transformer = new SectionTransformer();
        $result = $transformer->run($sectionConfig, [$apiEndpoint]);

        $inString = (bool) stristr($result, "/events/1: { get: { summary: test, description: sadsa, tags: {  }, responses: { 200: { description: 'test response' } } }, parameters: [{ name: name, description: 'Test descr', in: formData, type: string }] }");
        $this->assertEquals(true, $inString);
    }
}