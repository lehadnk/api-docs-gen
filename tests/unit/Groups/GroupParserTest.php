<?php
/**
 * Created by PhpStorm.
 * User: lehadnk
 * Date: 2019-02-02
 * Time: 09:38
 */

use ApiDocsGenerator\Config\ArrayConfigReader;
use ApiDocsGenerator\DataCollectors\PlainTextDataCollector;
use ApiDocsGenerator\Endpoints\ApiEndpoint;
use ApiDocsGenerator\Groups\ApiGroup;
use ApiDocsGenerator\Groups\ApiGroupParser;
use ApiDocsGenerator\RequestParameters\StringRequestParameter;
use ApiDocsGenerator\Transformers\SectionTransformer;
use UnitTests\AbstractTestProjectCase;

class TestApiGroup extends ApiGroup
{
    public function getAllEvents()
    {
        return new ApiEndpoint([
            'method' => 'GET',
            'uri' => '/events/1',
            'description' => 'sadsa',
            'summary' => 'test',
            'inputParams' => [

            ],
            'responseExamples' => [
                new PlainTextDataCollector(200, 'test response'),
            ]
        ]);
    }

    public function getOneEvent()
    {
        return new ApiEndpoint([
            'method' => 'GET',
            'uri' => '/events/1',
            'description' => 'sadsa',
            'summary' => 'test',
            'inputParams' => [],
            'responseExamples' => [
                new PlainTextDataCollector(200, 'test response'),
            ]
        ]);
    }
}

class UserSimpleOrdersApiDocs extends ApiGroup
{
    public function all() {
        return new ApiEndpoint([
            'method' => 'get',
            'uri' => '/simple-orders/',
            'summary' => "Gets the information about user's past orders",
            'tags' => ['simple order'],
            'inputParams' => [

            ],
            'responseExamples' => [
                new PlainTextDataCollector(200, 'test response'),
            ]
        ]);
    }

    public function add() {
        return new ApiEndpoint([
            'method' => 'post',
            'uri' => '/simple-orders',
            'summary' => "Adds simple order to the user",
            'tags' => ['simple order'],
            'inputParams' => [

            ],
            'responseExamples' => [
                new PlainTextDataCollector(200, 'test response'),
            ]
        ]);
    }

    public function one() {
        return new ApiEndpoint([
            'method' => 'get',
            'uri' => '/simple-orders/{uuid}',
            'summary' => "Gets the information about some concrete order",
            'tags' => ['simple order'],
            'inputParams' => [

            ],
            'responseExamples' => [
                new PlainTextDataCollector(200, 'test response'),
            ]
        ]);
    }
}

class GroupParserTest extends AbstractTestProjectCase
{

    public function testRead()
    {
        $parser = new ApiGroupParser();
        $group = new TestApiGroup();

        $endpoints = $parser->getEndpoints($group);

        $this->assertEquals(2, count($endpoints));
    }

    public function testMethodsWithSameUri()
    {
        $parser = new ApiGroupParser();
        $group = new UserSimpleOrdersApiDocs();

        $endpoints = $parser->getEndpoints($group);

        $this->assertEquals(3, count($endpoints));
    }
}
