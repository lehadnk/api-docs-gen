<?php
/**
 * Created by PhpStorm.
 * User: lehadnk
 * Date: 2019-02-02
 * Time: 20:56
 */

namespace UnitTests\Transformers;

use ApiDocsGenerator\FileLister;
use UnitTests\AbstractTestProjectCase;

class FileListerTest extends AbstractTestProjectCase
{
    public function testRun()
    {
        $fileLister = new FileLister();
        $fileList = $fileLister->getFilesInPath($this->getTestProjectDir().'/test-project');

        $this->assertStringContainsString('ProductsApi', $fileList[0]);
    }
}