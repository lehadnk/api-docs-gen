<?php
/**
 * Created by PhpStorm.
 * User: lehadnk
 * Date: 2019-02-02
 * Time: 20:56
 */

namespace UnitTests\Transformers;

use ApiDocsGenerator\DataCollectors\PlainTextDataCollector;
use ApiDocsGenerator\Endpoints\ApiEndpoint;
use ApiDocsGenerator\SwaggerGenerator;
use ApiDocsGenerator\Transformers\SectionTransformer;
use UnitTests\AbstractTestProjectCase;

class SwaggerGeneratorTest extends AbstractTestProjectCase
{
    public function testGenerateYml()
    {
        $config = $this->getTestConfig();

        $swaggerGenerator = new SwaggerGenerator($config);
        $contents = $swaggerGenerator->generateYaml('user-api', 'ru');

        $this->assertStringContainsString('/events/1: { get:', $contents);
        $this->assertStringContainsString('/events: { get:', $contents);
    }

    public function testGenerateDocs()
    {
        $config = $this->getTestConfig();

        $swaggerGenerator = new SwaggerGenerator($config);
        $swaggerGenerator->generateDocs();

        $this->assertFileExists($this->getTestProjectDir().'/test-project/docs/specification.user-api.en.yml');
    }
}