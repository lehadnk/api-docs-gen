<?php
/**
 * Created by PhpStorm.
 * User: lehadnk
 * Date: 2019-02-02
 * Time: 09:27
 */

return [
    'languages' => ['en', 'ru', 'cn'],
    'outputDirectory' => __DIR__.'/test-project/docs',
    'sections' => [
        'user-api' => [
            'host' => 'http://localhost',
            'version' => '1.0',
            'scheme' => 'https',
            'base_path' => 'user-api',
            'title' => 'User API',
            'description' => 'User API',
            'endpointDefinitionsPath' => __DIR__.'/test-project',
            'securityDefinitions' => [
                'api_token' => [
                    'type' => 'apiKey',
                    'name' => 'X-UserApi-Access-Token',
                    'in' => 'header',
                ],
            ]
        ],
        'merchant-api' => [
            'host' => 'http://localhost',
            'version' => '1.0',
            'scheme' => 'https',
            'base_path' => 'merchant-api',
            'title' => 'Merchant API',
            'description' => 'Merchant API',
            'endpointDefinitionsPath' => __DIR__.'/test-project',
            'securityDefinitions' => [
            ]
        ]
    ],
];