<?php

use ApiDocsGenerator\DataCollectors\PlainTextDataCollector;
use ApiDocsGenerator\Endpoints\ApiEndpoint;
use ApiDocsGenerator\Groups\ApiGroup;

/**
 * Created by PhpStorm.
 * User: lehadnk
 * Date: 2019-02-04
 * Time: 19:24
 */

class ProductsApi extends ApiGroup
{
    public function getOneEvent()
    {
        return new ApiEndpoint([
            'method' => 'GET',
            'uri' => '/events/1',
            'description' => 'sadsa',
            'summary' => 'test',
            'inputParams' => [],
            'responseExamples' => [
                new PlainTextDataCollector(200, 'test response'),
            ]
        ]);
    }

    public function getAllEvents()
    {
        return new ApiEndpoint([
            'method' => 'GET',
            'uri' => '/events',
            'description' => 'test description',
            'summary' => 'test',
            'inputParams' => [],
            'responseExamples' => [
                new PlainTextDataCollector(200, 'all events test response'),
            ]
        ]);
    }
}