<?php
/**
 * Created by PhpStorm.
 * User: lehadnk
 * Date: 2019-02-02
 * Time: 10:06
 */

namespace ApiDocsGenerator\DataCollectors;


class DataCollectorResponse
{
    /**
     * Associative array of all the response headers
     * @var array
     */
    public $headers;

    /**
     * Response contents
     * @var string|null
     */
    public $content;

    /**
     * Response status code
     * @var int
     */
    public $status;

    public function __construct(array $headers, string $content, int $status)
    {
        $this->headers = $headers;
        $this->content = $content;
        $this->status = $status;
    }
}