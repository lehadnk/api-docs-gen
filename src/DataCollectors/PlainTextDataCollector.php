<?php
/**
 * Created by PhpStorm.
 * User: lehadnk
 * Date: 2019-02-02
 * Time: 10:08
 */

namespace ApiDocsGenerator\DataCollectors;


class PlainTextDataCollector implements IDataCollector
{
    /**
     * @var int
     */
    private $status;
    /**
     * @var string
     */
    private $content;
    /**
     * @var array
     */
    private $headers;

    public function __construct(int $status, string $content, array $headers = [])
    {
        $this->status = $status;
        $this->content = $content;
        $this->headers = $headers;
    }

    public function getRequestData(): DataCollectorResponse
    {
        return new DataCollectorResponse($this->headers, $this->content, $this->status);
    }
}