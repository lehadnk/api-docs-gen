<?php
/**
 * Created by PhpStorm.
 * User: lehadnk
 * Date: 2019-02-07
 * Time: 14:24
 */

namespace ApiDocsGenerator\DataCollectors;


class EmptyResponseDataCollector implements IDataCollector
{
    public function getRequestData(): DataCollectorResponse
    {
        return new DataCollectorResponse([], '', 204);
    }
}