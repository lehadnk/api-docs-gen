<?php
/**
 * Created by PhpStorm.
 * User: lehadnk
 * Date: 2019-02-05
 * Time: 20:45
 */

namespace ApiDocsGenerator\DataCollectors;


use ApiDocsGenerator\Exceptions\ResponseExampleConfigurationException;

class FileDataCollector implements IDataCollector
{
    private $headers;
    private $content;
    private $statusCode;

    public function __construct(string $fileName)
    {
        if (!is_file($fileName)) {
            throw new ResponseExampleConfigurationException("No response file found: $fileName");
        }

        $data = file_get_contents($fileName);
        $json = json_decode($data);
        if (!json_last_error() === JSON_ERROR_NONE) {
            throw new ResponseExampleConfigurationException("Error parsing json data from $fileName");
        }

        $this->headers = (array) $json->headers ?? [];
        $this->content = $json->content ?? null;

        $formattedJson = json_decode($this->content);
        if (json_last_error() === JSON_ERROR_NONE) {
            $this->content = json_encode($formattedJson, JSON_PRETTY_PRINT);
        }

        $this->statusCode = $json->statusCode ?? 204;
    }

    public function getRequestData(): DataCollectorResponse
    {
        return new DataCollectorResponse($this->headers, $this->content, $this->statusCode);
    }
}