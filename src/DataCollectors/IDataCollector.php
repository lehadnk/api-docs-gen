<?php
/**
 * Created by PhpStorm.
 * User: lehadnk
 * Date: 2019-02-02
 * Time: 10:04
 */

namespace ApiDocsGenerator\DataCollectors;

interface IDataCollector
{
    public function getRequestData(): DataCollectorResponse;
}