<?php
/**
 * Created by PhpStorm.
 * User: lehadnk
 * Date: 2019-02-04
 * Time: 18:29
 */

namespace ApiDocsGenerator\Groups;


use ApiDocsGenerator\Endpoints\IApiEndpoint;
use ApiDocsGenerator\Exceptions\GroupConfigurationException;

class ApiGroupParser
{
    public function getEndpoints(ApiGroup $group): array
    {
        $methods = get_class_methods(get_class($group));

        $endpoints = [];
        foreach ($methods as $method) {
            $endpoint = $group->$method();

            if (!$endpoint instanceof IApiEndpoint) {
                throw new GroupConfigurationException();
            }

            $endpoints[] = $endpoint;
        }

        return $endpoints;
    }
}