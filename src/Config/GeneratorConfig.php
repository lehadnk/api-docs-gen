<?php
/**
 * Created by PhpStorm.
 * User: lehadnk
 * Date: 2019-02-02
 * Time: 09:17
 */

namespace ApiDocsGenerator\Config;

class GeneratorConfig
{
    /**
     * @var string[]
     */
    public $languages;

    /**
     * @var SectionConfig[]
     */
    public $sections;

    /**
     * @var string
     */
    public $endpointDefinitionsPath;

    /**
     * @var string
     */
    public $outputDirectory;
}