<?php
/**
 * Created by PhpStorm.
 * User: lehadnk
 * Date: 2019-02-02
 * Time: 09:24
 */

namespace ApiDocsGenerator\Config;

class SectionConfig
{
    /**
     * @var string
     */
    public $name;

    /**
     * @var string
     */
    public $host;

    /**
     * @var string
     */
    public $version;

    /**
     * @var string
     */
    public $scheme;

    /**
     * @var string
     */
    public $base_path;

    /**
     * @var string
     */
    public $title;

    /**
     * @var string
     */
    public $description;

    /**
     * @var SecurityDefinitionConfig[]
     */
    public $securityDefinitions;

    /**
     * @var string
     */
    public $endpointDefinitionsPath;

    public function getUrl(): string
    {
        return $this->host.'/'.trim('/', $this->base_path);
    }
}