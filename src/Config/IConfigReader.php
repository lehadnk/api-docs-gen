<?php
/**
 * Created by PhpStorm.
 * User: lehadnk
 * Date: 2019-02-02
 * Time: 09:16
 */

namespace ApiDocsGenerator\Config;

interface IConfigReader
{
    public function read(string $filePath): GeneratorConfig;
}