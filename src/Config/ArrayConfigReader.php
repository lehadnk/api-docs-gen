<?php
/**
 * Created by PhpStorm.
 * User: lehadnk
 * Date: 2019-02-02
 * Time: 09:24
 */

namespace ApiDocsGenerator\Config;

use ApiDocsGenerator\Exceptions\ConfigParseErrorException;

class ArrayConfigReader implements IConfigReader
{

    /**
     * @param string $filePath
     * @return GeneratorConfig
     * @throws ConfigParseErrorException
     */
    public function read(string $filePath): GeneratorConfig
    {
        try {
            /** @noinspection PhpIncludeInspection */
            $configData = include $filePath;
        } catch (\Exception $ex) {
            throw new ConfigParseErrorException("Unable to read config: ".$ex->getMessage());
        }

        try {
            $generatorConfig = new GeneratorConfig();

            $generatorConfig->outputDirectory = $configData['outputDirectory'];

            foreach($configData['languages'] as $language) {
                $generatorConfig->languages[] = $language;
            }

            foreach($configData['sections'] as $sectionKey => $sectionData) {
                $sectionConfig = new SectionConfig();
                $sectionConfig->name = $sectionKey;
                $sectionConfig->title = $sectionData['title'];
                $sectionConfig->description = $sectionData['description'];
                $sectionConfig->base_path = $sectionData['base_path'];
                $sectionConfig->host = $sectionData['host'];
                $sectionConfig->scheme = $sectionData['scheme'];
                $sectionConfig->version = $sectionData['version'];
                $sectionConfig->endpointDefinitionsPath = $sectionData['endpointDefinitionsPath'];
                $sectionConfig->securityDefinitions = [];
                foreach($sectionData['securityDefinitions'] as $definitionName => $definitionData) {
                    $securityDefinitionConfig = new SecurityDefinitionConfig();
                    $securityDefinitionConfig->name = $definitionData['name'];
                    $securityDefinitionConfig->type = $definitionData['type'];
                    $securityDefinitionConfig->in = $definitionData['in'];
                    $sectionConfig->securityDefinitions[$definitionName] = $definitionData;
                }
                $generatorConfig->sections[$sectionKey] = $sectionConfig;
            }

            return $generatorConfig;
        } catch (\Exception $ex) {
            throw new ConfigParseErrorException("Unable to read config data. Is it corrupt? ".$ex->getMessage());
        }
    }
}