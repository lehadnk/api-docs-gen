<?php
/**
 * Created by PhpStorm.
 * User: lehadnk
 * Date: 2019-02-02
 * Time: 10:47
 */

namespace ApiDocsGenerator\Transformers;


use ApiDocsGenerator\Config\SectionConfig;
use ApiDocsGenerator\Endpoints\IApiEndpoint;
use Symfony\Component\Yaml\Yaml;

class SectionTransformer
{
    /**
     * @param SectionConfig $config
     * @param IApiEndpoint[] $endpoints
     * @return string
     */
    public function run(SectionConfig $config, array $endpoints): string
    {
        $securityList = array_flip(array_keys($config->securityDefinitions));

        $endpointDefinitions = [];
        foreach ($endpoints as $endpoint) {
            if (!isset($endpointDefinitions[$endpoint->getUri()])) {
                $endpointDefinitions[$endpoint->getUri()] = $this->endpointToArray($endpoint);
            } else {
                $endpointDefinitions[$endpoint->getUri()] = array_merge($this->endpointToArray($endpoint), $endpointDefinitions[$endpoint->getUri()]);
            }
        }

        $sectionData = [
            'swagger' => '2.0',
            'info' => [
                'title' => $config->title,
                'description' => $config->description,
                'version' => $config->version,
            ],
            'host' => $config->host,
            'basePath' => $config->base_path,
            'schemes' => [$config->scheme],
            'produces' => ['application/json'],
            'securityDefinitions' => $config->securityDefinitions,
            'security' => $securityList,
            'paths' => $endpointDefinitions,
        ];

        return Yaml::dump($sectionData);
    }

    private function endpointToArray(IApiEndpoint $endpoint): array
    {
        $responseExamples = $endpoint->getResponseExamples();

        $responses = [];
        foreach($responseExamples as $example) {
            $response = $example->getRequestData();
            $responses[$response->status] = [
                'description' => $response->content,
            ];
        }

        $parameters = $endpoint->getInputParams();
        $inputParameters = [];
        foreach($parameters as $parameter) {
            $inputParameters[] = [
                'name' => $parameter->getName(),
                'description' => $parameter->getDescription(),
                'in' => $parameter->getIn(),
                'type' => $parameter->getType()
            ];
        }

        $key = strtolower($endpoint->getRequestMethod());
        $array = [
            $key => [
                'summary' => $endpoint->getSummary(),
                'description' => $endpoint->getDescription(),
                'tags' => $endpoint->getTags(),
                'responses' => $responses,
                'parameters' => $inputParameters,
            ]
        ];

        return $array;
    }
}