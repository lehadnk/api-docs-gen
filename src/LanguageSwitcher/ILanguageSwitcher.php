<?php
/**
 * Created by PhpStorm.
 * User: lehadnk
 * Date: 2019-02-12
 * Time: 15:37
 */

namespace ApiDocsGenerator\LanguageSwitcher;


interface ILanguageSwitcher
{
    public function setLanguage(string $locale);
}