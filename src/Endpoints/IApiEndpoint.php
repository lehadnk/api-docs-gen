<?php
/**
 * Created by PhpStorm.
 * User: lehadnk
 * Date: 2019-02-02
 * Time: 10:18
 */

namespace ApiDocsGenerator\Endpoints;


use ApiDocsGenerator\DataCollectors\IDataCollector;
use ApiDocsGenerator\RequestParameters\IRequestParameter;

interface IApiEndpoint
{
    public function addResponseExample(IDataCollector $collector);
    public function getRequestMethod(): string;
    public function getUri(): string;
    public function getDescription(): string;
    public function getSummary(): string;

    /**
     * @return string[]
     */
    public function getTags(): array;

    /**
     * @return IRequestParameter[]
     */
    public function getInputParams(): array;

    /**
     * @return IDataCollector[]
     */
    public function getResponseExamples(): array;
}