<?php
/**
 * Created by PhpStorm.
 * User: lehadnk
 * Date: 2019-02-02
 * Time: 10:21
 */

namespace ApiDocsGenerator\Endpoints;


use ApiDocsGenerator\DataCollectors\IDataCollector;
use ApiDocsGenerator\Exceptions\EndpointConfigurationException;
use ApiDocsGenerator\RequestParameters\IRequestParameter;

class ApiEndpoint implements IApiEndpoint
{
    private $method;
    private $uri;
    private $summary;
    private $description;
    private $inputParams = [];
    private $responseExamples = [];
    private $tags = [];

    /**
     * ApiEndpoint constructor.
     * @param array $endpointData
     * @throws EndpointConfigurationException
     */
    public function __construct(array $endpointData)
    {
        $this->validate($endpointData);

        $this->method = $endpointData['method'];
        $this->uri = $endpointData['uri'];
        $this->summary = $endpointData['summary'];
        $this->description = $endpointData['description'] ?? '';
        $this->tags = $endpointData['tags'] ?? [];

        if (isset($endpointData['responseExamples'])) {
            $this->addCollectors($endpointData['responseExamples']);
        }
        if (isset($endpointData['inputParams'])) {
            $this->addInputParams($endpointData['inputParams']);
        }
    }

    private function addCollectors(array $collectorsList)
    {
        foreach ($collectorsList as $collector)
        {
            if (!$collector instanceof IDataCollector) {
                throw new EndpointConfigurationException("Error with endpoint configuration for ".__CLASS__.": attempting to add response example ".get_class($collector)." but it does not implement IDataCollector");
            }
            $this->responseExamples[] = $collector;
        }
    }

    private function addInputParams(array $inputParamsList)
    {
        foreach ($inputParamsList as $inputParam) {
            if (!$inputParam instanceof IRequestParameter) {
                throw new EndpointConfigurationException("Error with endpoint configuration for ".__CLASS__.": attempting to add input parameter ".get_class($inputParam)." but it does not implement IRequestParameter");
            }
            $this->inputParams[] = $inputParam;
        }
    }

    private function validate(array $endpointData)
    {
        if (!isset($endpointData['method'])) {
            throw new EndpointConfigurationException("Error with endpoint configuration for ".__CLASS__.": missing 'method' field");
        }
        if (!isset($endpointData['uri'])) {
            throw new EndpointConfigurationException("Error with endpoint configuration for ".__CLASS__.": missing 'uri' field");
        }
        if (!isset($endpointData['summary'])) {
            throw new EndpointConfigurationException("Error with endpoint configuration for ".__CLASS__.": missing 'summary' field");
        }
    }

    public function addResponseExample(IDataCollector $collector)
    {
        $this->responseExamples[] = $collector;
    }

    public function getRequestMethod(): string
    {
        return $this->method;
    }

    public function getUri(): string
    {
        return $this->uri;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function getSummary(): string
    {
        return $this->summary;
    }

    public function getInputParams(): array
    {
        return $this->inputParams;
    }

    public function getResponseExamples(): array
    {
        return $this->responseExamples;
    }

    /**
     * @return string[]
     */
    public function getTags(): array
    {
        return $this->tags;
    }
}