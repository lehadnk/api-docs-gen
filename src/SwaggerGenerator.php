<?php
/**
 * Created by PhpStorm.
 * User: lehadnk
 * Date: 2019-02-02
 * Time: 09:13
 */

namespace ApiDocsGenerator;

use ApiDocsGenerator\Config\GeneratorConfig;
use ApiDocsGenerator\Groups\ApiGroupParser;
use ApiDocsGenerator\LanguageSwitcher\ILanguageSwitcher;
use ApiDocsGenerator\Transformers\SectionTransformer;

class SwaggerGenerator
{
    /**
     * @var GeneratorConfig
     */
    private $config;

    /**
     * @var bool
     */
    public $dry = false;

    public function __construct(GeneratorConfig $config)
    {
        $this->config = $config;
    }

    public function generateDocs(ILanguageSwitcher $languageSwitcher = null)
    {
        foreach($this->config->sections as $sectionName => $sectionConfig) {
            foreach($this->config->languages as $language) {

                if ($languageSwitcher !== null) {
                    $languageSwitcher->setLanguage($language);
                }

                $contents = $this->generateYaml($sectionName, $language);
                if (!$this->dry) {
                    file_put_contents($this->config->outputDirectory."/specification.$sectionName.$language.yml", $contents);
                }
            }
        }
    }

    public function generateYaml(string $section, string $language): string
    {
        $sectionConfig = $this->config->sections[$section];

        $fileLister = new FileLister();
        $fileList = $fileLister->getFilesInPath($sectionConfig->endpointDefinitionsPath);

        $groupParser = new ApiGroupParser();

        $endpoints = [];
        foreach ($fileList as $file) {
            /** @noinspection PhpIncludeInspection */
            require_once($file);

            $class = basename($file, '.php');
            $group = new $class;
            $endpoints = array_merge($endpoints, $groupParser->getEndpoints($group));
        }

        $transformer = new SectionTransformer();
        $contents = $transformer->run($sectionConfig, $endpoints);

        return $contents;
    }
}