<?php
/**
 * Created by PhpStorm.
 * User: lehadnk
 * Date: 2019-02-06
 * Time: 20:21
 */

namespace ApiDocsGenerator\DocsGenerators;


use function Couchbase\defaultDecoder;
use Symfony\Component\Yaml\Yaml;

class YmlDocsGenerator
{
    private $template = <<<code
public function {name}() {
    return new ApiEndpoint([
        'method' => '{method}',
        'uri' => '{uri}',
        'summary' => __("{summary}"),
        'tags' => [__('{tag}')],
        'inputParams' => [
            {inputParams}
        ],
        'responseExamples' => [
            
        ]
    ]);
}
code;


    public function generate(string $inputFile, string $outputFile)
    {
        $content = file_get_contents($inputFile);
        $data = Yaml::parse($content);

        $paths = $data['paths'];
        ksort($paths);

        $string = '';
        foreach ($paths as $pathName => $path) {
            foreach ($path as $method => $request) {
                $string .= $this->getResponseData($pathName, $method, $request).PHP_EOL.PHP_EOL;
            }
        }

        file_put_contents($outputFile, $string);
    }

    private function getResponseData(string $path, string $method, array $request)
    {
        $tag = $request['tags'][0] ?? '';
        $template = $this->template;

        if (isset($request['parameters'])) {
            $inputParams = $this->getInputParameters($request['parameters']);
        } else {
            $inputParams = '';
        }

        $template = str_replace('{name}', strtolower($method), $template);
        $template = str_replace('{method}', strtolower($method), $template);
        $template = str_replace('{uri}', $path, $template);
        $template = str_replace('{summary}', $request['summary'], $template);
        $template = str_replace('{tags}', $tag, $template);
        $template = str_replace('{inputParams}', $inputParams, $template);

        return $template;
    }

    private function getInputParameters($parameters)
    {
        $inputParameters = '';

        foreach($parameters as $parameter) {
            if ($parameter['type'] === 'string') {
                $inputParameters .= "new StringRequestParameter('{$parameter['name']}', __('{$parameter['description']}')),".PHP_EOL;
            }
            if ($parameter['type'] === 'int') {
                $inputParameters .= "new IntegerRequestParameter('{$parameter['name']}', __('{$parameter['description']}')),".PHP_EOL;
            }
            if ($parameter['type'] === 'number') {
                $inputParameters .= "new NumberRequestParameter('{$parameter['name']}', __('{$parameter['description']}')),".PHP_EOL;
            }
        }

        return $inputParameters;
    }
}