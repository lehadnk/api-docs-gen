<?php
/**
 * Created by PhpStorm.
 * User: lehadnk
 * Date: 2019-02-06
 * Time: 01:33
 */

namespace ApiDocsGenerator\RequestParameters;


interface IRequestParameter
{
    const IN_FORMDATA = 'formData';
    const IN_PATH = 'path';

    public function getName(): string;
    public function getDescription(): string;
    public function getType(): string;
    public function getIn(): string;
}