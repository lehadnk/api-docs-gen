<?php
/**
 * Created by PhpStorm.
 * User: lehadnk
 * Date: 2019-02-06
 * Time: 01:34
 */

namespace ApiDocsGenerator\RequestParameters;


abstract class RequestParameterAbstract implements IRequestParameter
{
    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $description;

    /**
     * @var string
     */
    private $in;

    public function __construct(string $name, string $description, string $in = 'formData')
    {
        $this->name = $name;
        $this->description = $description;
        $this->in = $in;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function getIn(): string
    {
        return $this->in;
    }
}