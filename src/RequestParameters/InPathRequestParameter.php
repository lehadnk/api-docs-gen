<?php
/**
 * Created by PhpStorm.
 * User: lehadnk
 * Date: 2019-02-06
 * Time: 01:35
 */

namespace ApiDocsGenerator\RequestParameters;


class InPathRequestParameter extends RequestParameterAbstract
{
    public function getType(): string
    {
        return 'string';
    }

    public function getIn(): string
    {
        return IRequestParameter::IN_PATH;
    }
}