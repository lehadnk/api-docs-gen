<?php
/**
 * Created by PhpStorm.
 * User: lehadnk
 * Date: 2019-02-04
 * Time: 19:13
 */

namespace ApiDocsGenerator;


class FileLister
{
    /**
     * @param string $path
     * @return string[]
     */
    public function getFilesInPath(string $path): array
    {
        if (is_file($path)) return [$path];

        $directory = new \RecursiveDirectoryIterator($path);
        $iterator = new \RecursiveIteratorIterator($directory);
        $fileList = new \RegexIterator($iterator, '/^.+\.(?:php)$/i', \RegexIterator::GET_MATCH);

        $list = [];
        foreach ($fileList as $file) {
            $list[] = $file[0];
        }

        return $list;
    }
}